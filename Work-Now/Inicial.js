import React, {Component} from 'react';
import {Platform, StyleSheet, Text, 
        View, StatusBar, Image, TouchableOpacity} from 'react-native';

class Inicial extends Component<{}> {

  render() {
    return (
      <View style = {styles.container}>
        <Image style={{width:100, height: 100, resizeMode: "contain"}}
          source={require("../imagens/work-now-LOGO.png")}/>


          <TouchableOpacity style={styles.botao} 
          onPress={()=>this.props.navigation.navigate("Login")}>
            <Text style={styles.textoBotao}>Logar</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.botao} 
          onPress={()=>this.props.navigation.navigate("Cadastro")}>
            <Text style={styles.textoBotao}>Cadastrar</Text>
          </TouchableOpacity> 

      </View>

    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#534bae',
  },
  botao: {
    marginTop:10,
    backgroundColor:"#000000",
    borderRadius: 5,
    width:100,
    height:40
  },
  textoBotao:{
    fontSize:16,
    color:"#ffffff",
    textAlign:'center',

  }
});
export default Inicial;
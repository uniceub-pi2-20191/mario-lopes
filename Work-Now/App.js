import React, {Component} from 'react';
import {Platform, StyleSheet, Text, 
        View, StatusBar, Image} from 'react-native';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';        

import Inicial from "./buscar/paginas/Inicial";
import Login from "./buscar/paginas/Login";
import Teste from "./buscar/paginas/Teste";
import Cadastro from "./buscar/paginas/Cadastro";
import DadosPessoais from "./buscar/paginas/DadosPessoais";



const Navigator = createSwitchNavigator({
    Inicial: {
      screen:Inicial
    },
    Login:{
      screen: Login
    },
    Teste: {
      screen: Teste
    },
    Cadastro: {
      screen: Cadastro
    },
    DadosPessoais:{
      screen: DadosPessoais
    },

});

export default createAppContainer(Navigator);

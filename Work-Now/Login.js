import React, {Component} from 'react';
import {Platform, StyleSheet, Text, 
        View, StatusBar, Image, TextInput, 
        TouchableOpacity, Alert} from 'react-native';
import firebase from "react-native-firebase";    
import AsyncStorage from 'react-native';    

class Login extends Component<{}> {

constructor(props) {
    super(props);

    this.state = {
      email: '',
      senha: '',
      isAuthenticated:true,
    };
  };

  login = async () => {
    const { email, senha } = this.state;

    try {
      const user = await firebase.auth()
      .signInWithEmailAndPassword(email, senha);
        this.props.navigation.navigate("Teste");
    } catch (err){
      Alert.alert("Email ou senha incorretos!");
    }
  }
  render() {
    return (
      <View style = {styles.container}>
      <Image style={{width:100, height: 100, resizeMode: "contain"}}
          source={require("../imagens/work-now-LOGO.png")}/>
      
      <TextInput style = {styles.inputBox}
                 placeholder = "Email"
                 value={this.state.email}
                 onChangeText={email => this.setState({email})}/>

      <TextInput style = {styles.inputBox}
                 placeholder = "Senha"
                 secureTextEntry
                 value={this.state.senha}
                 onChangeText={senha => this.setState({senha})}/>

      <TouchableOpacity style={styles.botao} onPress={this.login}>
            <Text style={styles.textoBotao}>Entrar</Text>
          </TouchableOpacity>

      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#534bae',
  },
  inputBox: {
    width:300,
    backgroundColor: "#fff7ff",
    borderRadius: 25,
    paddingHorizontal:10,
    marginVertical: 5
  },
  textoBotao:{
    fontSize:16,
    color:"#ffffff",
    textAlign:'center',
  },
  botao: {
    marginTop:10,
    backgroundColor:"#000000",
    borderRadius: 5,
    width:100,
    height:40
  },
});
export default Login;
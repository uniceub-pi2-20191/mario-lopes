import React, {Component} from 'react';
import {Text, View, Button} from 'react-native';
import {createStackNavigator, createAppContainer} from 'react-navigation';
import Cadastro from "./src/Cadastro";

class TelaInicial extends Component{
  render () {
    return(
      <View style={{flex:1, justifyContent: "center"}}>
        <View style = {{alignItems: "center"}}>
          <Text style={{fontSize: 40}}>TELA INICIAL</Text>
        </View>
        <View style ={{margin:20}}>
          <Button
            title = "Ir tela Perfil"
            onPress = { () => this.props.navigation.navigate("Perfil")}/>
          </View>  
          <View style ={{margin:20}}>
          <Button
            title = "Ir tela Cadastro"
            onPress = { () => this.props.navigation.navigate("Cadastro")}/>
          </View>

      </View>
      );
  }
}

class TelaCadastro extends Component{
  render () {
    return(
      <View style={{flex:1, justifyContent: "center"}}>
        <View style = {{alignItems: "center"}}>
          <Text style={{fontSize: 40}}>TELA CADASTRO</Text>
        </View>
         <View style ={{margin:20}}>
          <Button
            title = "Ir tela Perfil"
            onPress = { () => this.props.navigation.navigate("Perfil")}/>
          </View> 
          <View>
            <Cadastro/> 
          </View>
      </View>
      );
  }
}

class TelaPerfil extends Component{
  render () {
    return(
      <View style={{flex:1, justifyContent: "center"}}>
        <View style = {{alignItems: "center"}}>
          <Text style={{fontSize: 40}}>TELA PERFIL</Text>
        </View>

          <View style ={{margin:20}}>
          <Button
            title = "Ir tela Cadastro"
            onPress = { () => this.props.navigation.navigate("Cadastro")}/>
          </View>  
      </View>
      );
  }
}
const Navegacao = createStackNavigator (
  {
    Inicio: {
      screen: TelaInicial
    },
    Cadastro: {
      screen: TelaCadastro
    },
    Perfil: {
      screen: TelaPerfil
    }
  },
  {
    initialRouteName: "Inicio"
  }
);

const AppContainer = createAppContainer(Navegacao);

export default class App extends Component{
  render() {
    return <AppContainer/>;
      
  }
}
import React, { Component } from 'react';
import { AppRegistry, Image, Text, View } from 'react-native';

export default class Testes extends Component {
  render() {
    let pic = {
      uri: 'https://www.natgeo.pt/sites/portugal/files/styles/image_1900/public/gr-shark-alphabet-gallery_NationalGeographic_649456.jpg'
    };
    let pic2 = {
          uri: 'https://abrilexame.files.wordpress.com/2016/09/size_960_16_9_20151019-14270-2o22ox.jpg?quality=70&strip=info&resize=680,453'
        };
    return (
    <View>
      <Image source={pic} style={{width: 193, height: 110}}/>
        <Text> TESTANDO TUBARÃO 01</Text>
      <Image source={pic2} style={{width: 193, height: 110}}/>
        <Text> TESTANDO TUBARÃO 02</Text>
    </View>
    );
  }
}
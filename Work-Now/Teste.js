import React, {Component} from 'react';
import {Platform, StyleSheet, Text, 
        View, StatusBar, TouchableOpacity} from 'react-native';
import Database from "./Database";

class Teste extends Component<{}> {

  render() {
    return (
      <View style = {styles.container}>

      <View style = {styles.database1}>
      <Database/>
      </View>
        <Text style = {styles.texto1}>LOGADO COM SUCESSO!</Text>


        <TouchableOpacity style={styles.botao}
        onPress={()=>this.props.navigation.navigate("DadosPessoais")}>
            <Text style={styles.textoBotao}>Alterar perfil</Text>
          
      </TouchableOpacity>
      
      </View>

    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#534bae',
   },
  textoBotao:{
    fontSize:16,
    color:"#ffffff",
    textAlign:'center',
  },
  botao: {
    marginTop:10,
    backgroundColor:"#000000",
    borderRadius: 5,
    width:100,
    height:40
  },
  texto1: {
    color: "#ffffff",
  },
  database1:{
    flex:1,
  }
});
export default Teste;
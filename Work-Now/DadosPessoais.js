import React, {Component} from 'react';
import {Platform, StyleSheet, Text, 
        View, StatusBar, TouchableOpacity, Image,
        TextInput, Alert} from 'react-native';
import firebase from "react-native-firebase"; 


class DadosPessoais extends Component<{}> {

  constructor(props) {
    super(props);

    this.state = {
      nome: '',
      formação: '',
      isAuthenticated:true,
    };
  };
  salvar = async () => {
    const { nome, formacao } = this.state;

    try {
      const user = await firebase.database()
      .ref("Users").set({ nome, formacao});
        this.props.navigation.navigate("Teste");
    } catch (err){
      Alert.alert("Algo Errado");
    }
    
  }

  render() {
    return (
      <View style = {styles.container}>

      <Image style={{width:50, height: 50, resizeMode: "contain"}}
          source={require("../imagens/work-now-LOGO.png")}/>

       <TextInput style = {styles.inputBox}
                 placeholder = "Nome Completo"
                 value={this.state.nome}
                 onChangeText={nome => this.setState({nome})}/>

      <TextInput style = {styles.inputBox}
                 placeholder = "Formação"
                 value={this.state.formacao}
                 onChangeText={formacao => this.setState({formacao})}/>

      <TouchableOpacity style={styles.botao}onPress={this.salvar}>
            <Text style={styles.textoBotao}>Salvar</Text>
      </TouchableOpacity>

      </View>

    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#534bae',
   },
   inputBox: {
    width:300,
    height: 40,
    backgroundColor: "#fff7ff",
    borderRadius: 25,
    paddingHorizontal:10,
    marginVertical: 10,
  },
  botao: {
    marginTop:10,
    backgroundColor:"#000000",
    borderRadius: 5,
    width:100,
    height:40
  },
  textoBotao:{
    fontSize:16,
    color:"#ffffff",
    textAlign:'center',
  },
});
export default DadosPessoais;